#include "canvascleaner.h"

using namespace std;

int main(int argc, char* argv[] )
{
    TObjArray* canArray = new TObjArray();
    TCanvas* canvas = 0;

    TString masterCard = "";

    for (int i=1; i<argc; i++)
    {
        TString card = string(argv[i]);
        if(card.EndsWith(".dat"))
        {
            canvas = canvascleaner(card,i,argc);
            if(masterCard=="") masterCard = card;
        }
        if(/*argc > 2 && */canvas)
        {
            canvas->SetName(TString(canvas->GetName())+"_"+TString::Itoa(i,10));
            //Following two lines specific to overlaying masses
            //TString mass = card.Contains("500") ? "500" : card.Contains("1000") ? "1000" : card.Contains("3000") ? "3000" : "";
            //canvas->SetName("canvas_" + mass);
            if(canvas->GetListOfPrimitives()->GetEntries() > 0)
            {
                cout << "pushing back " << canvas->GetName() << endl;
                canArray->Add(canvas->Clone());
            }
        }
        else if(argc == 2) masterCard = card;
        else if(argc == 1)
        {
            cout << "please add path to at least one data card as argument" << endl;
            exit(0);
        }
    }

    /*if(argc > 2 && canArray->GetEntries() > 1)*/ canvas = canvascombiner(canArray,masterCard);

    TEnv env(masterCard);

    gPad->SetLogx(env.GetValue("logx",0));
    gPad->SetLogy(env.GetValue("logy",0));
    canvas->Modified();

    TString atlas = env.GetValue("atlas","");
    double atlasx = env.GetValue("atlasx",0.2);
    double atlasy = env.GetValue("atlasy",0.85);

    if     (atlas=="Work in Progress")      ATLASLabel(atlasx,atlasy,"Work in Progress",env.GetValue("atlassize",0.05));
    else if(atlas=="Internal")              ATLASLabel(atlasx,atlasy,"Internal",env.GetValue("atlassize",0.05));
    else if(atlas=="Preliminary")           ATLASLabel(atlasx,atlasy,"Preliminary",env.GetValue("atlassize",0.05));
    else if(atlas=="Simulation")            ATLASLabel(atlasx,atlasy,"Simulation",env.GetValue("atlassize",0.05));

    //Add up to 7 TLatex messages
    TLatex latex;
    latex.SetTextFont(42);
    latex.SetTextColor(1);
    latex.SetTextSize(0.045);
    if(env.Defined("latex"))    latex.DrawLatexNDC(env.GetValue("latexx",0.6), env.GetValue("latexy",0.85), env.GetValue("latex","#sqrt{s} = 13 TeV, 36.1 fb^{-1}"));
    if(env.Defined("sublatex")) latex.DrawLatexNDC(env.GetValue("sublatexx",0.6), env.GetValue("sublatexy",0.8), env.GetValue("sublatex","Z'#rightarrow #mu#mu"));
    if(env.Defined("subsublatex")) latex.DrawLatexNDC(env.GetValue("subsublatexx",0.6), env.GetValue("subsublatexy",0.75), env.GetValue("subsublatex","awkward silence"));
    if(env.Defined("subsubsublatex")) latex.DrawLatexNDC(env.GetValue("subsubsublatexx",0.6), env.GetValue("subsubsublatexy",0.75), env.GetValue("subsubsublatex","Uhm"));
    if(env.Defined("subsubsubsublatex")) latex.DrawLatexNDC(env.GetValue("subsubsubsublatexx",0.6), env.GetValue("subsubsubsublatexy",0.75), env.GetValue("subsubsubsublatex","Uhmm"));
    if(env.Defined("subsubsubsubsublatex")) latex.DrawLatexNDC(env.GetValue("subsubsubsubsublatexx",0.6), env.GetValue("subsubsubsubsublatexy",0.75), env.GetValue("subsubsubsubsublatex","Uhmmm"));
    if(env.Defined("subsubsubsubsubsublatex")) latex.DrawLatexNDC(env.GetValue("subsubsubsubsubsublatexx",0.6), env.GetValue("subsubsubsubsubsublatexy",0.75), env.GetValue("subsubsubsubsubsublatex","Uhmmmm"));

    //Add TLines all over the place
    if(env.Defined("tlineminx"))
    {
        TLine tline;
        tline.SetLineColor(env.GetValue("tlinecolor",1));
        tline.SetLineStyle(env.GetValue("tlinestyle",1));
        tline.SetLineWidth(env.GetValue("tlinewidth",0.7));
        tline.DrawLineNDC(env.GetValue("tlineminx",0.),env.GetValue("tlineminy",0.),env.GetValue("tlinemaxx",0.5),env.GetValue("tlinemaxy",0.5));
    }
    if(env.Defined("subtlineminx"))
    {
        TLine subtline;
        subtline.SetLineColor(env.GetValue("subtlinecolor",1));
        subtline.SetLineStyle(env.GetValue("subtlinestyle",1));
        subtline.SetLineWidth(env.GetValue("subtlinewidth",0.7));
        subtline.DrawLineNDC(env.GetValue("subtlineminx",0.),env.GetValue("subtlineminy",0.),env.GetValue("subtlinemaxx",0.5),env.GetValue("subtlinemaxy",0.5));
    }
    if(env.Defined("subsubtlineminx"))
    {
        TLine subsubtline;
        subsubtline.SetLineColor(env.GetValue("subsubtlinecolor",1));
        subsubtline.SetLineStyle(env.GetValue("subsubtlinestyle",1));
        subsubtline.SetLineWidth(env.GetValue("subsubtlinewidth",0.7));
        subsubtline.DrawLineNDC(env.GetValue("subsubtlineminx",0.),env.GetValue("subsubtlineminy",0.),env.GetValue("subsubtlinemaxx",0.5),env.GetValue("subsubtlinemaxy",0.5));
    }

    TString write = masterCard;
    write.ReplaceAll(".dat",".root");

    canvaswriter(canvas,write);

    return 0;
}

TCanvas* canvascleaner(TString card, int which, int total)
{
    if(card=="") exit(0);
    TEnv env(card);

    TCanvas* canvas = 0;
    int canvasx = env.GetValue("canvasx",800);
    int canvasy = env.GetValue("canvasy",600);

    if(TString(env.GetValue("file","")).EndsWith(".txt"))
    {
        TChain* chain = new TChain(env.GetValue("name",""));
        string line;
        ifstream txtfile(env.GetValue("file",""));
        while(getline(txtfile,line))
        {
            chain->Add(TString(line));
        }
        txtfile.close();

        cout << "Built TChain from list of TTree objects called " << env.GetValue("name","") << endl;
        canvas = new TCanvas("canvas_"+TString(env.GetValue("name","")),"canvas_"+TString(env.GetValue("name","")),canvasx,canvasy);

        TString var = env.GetValue("var","");
        TString cond = env.GetValue("cond","");

        if(env.Defined("var")) var = env.GetValue("var","");

        if(var=="")
        {
            cout << "No var provided to draw this chain!" << endl;
            exit(0);
        }
        if(var.Contains(":"))
        {
            int n = chain->Draw(var,cond,"goff");
            cout << "n = " << n << endl;
            TGraph* graph = new TGraph(n,chain->GetVal(1),chain->GetVal(0));
            graph->SetName("graph_"+TString(env.GetValue("name","")));
            graph->Draw("apl");
        }
        else
        {
            chain->Draw(var+">>"+env.GetValue("hist","histogram_"+TString::Itoa(which,10))+env.GetValue("binning",""),cond);
            TH1D* histogram = (TH1D*)gROOT->FindObject(env.GetValue("hist","histogram_"+TString::Itoa(which,10)));
            histogram->SetDirectory(0);
            histogram->Draw("hist");
        }
    }

    else
    {

        TFile* file = new TFile(env.GetValue("file",""),"read");
        if(file->IsZombie())
        {
            cout << "File " << env.GetValue("file","") << " does not exist!" << endl;
            canvas = new TCanvas("canvas_"+TString(env.GetValue("name","")),"canvas_"+TString(env.GetValue("name","")),canvasx,canvasy);
            return canvas; //exit(0);
        }
        else cout << "Opening file " << env.GetValue("file","") << endl;

        if(!file->Get(env.GetValue("name","")))
        {
            cout << "Object " << env.GetValue("name","") << " does not exist!" << endl;
            canvas = new TCanvas("canvas_"+TString(env.GetValue("name","")),"canvas_"+TString(env.GetValue("name","")),canvasx,canvasy);
            return canvas; //exit(0);
        }
        else if(file->Get(env.GetValue("name",""))->InheritsFrom("TCanvas"))
        {
            cout << "Found canvas " << env.GetValue("name","") << endl;
            canvas = (TCanvas*)file->Get(env.GetValue("name",""))->Clone();
            file->Close();
        }
        else if(file->Get(env.GetValue("name",""))->InheritsFrom("TH1"))
        {
            cout << "Found histogram " << env.GetValue("name","") << endl;
            TH1D* histogram = (TH1D*)file->Get(env.GetValue("name",""))->Clone();
            histogram->SetDirectory(0);
            canvas = new TCanvas("canvas_"+TString(env.GetValue("name","")),"canvas_"+TString(env.GetValue("name","")),canvasx,canvasy);
            histogram->Draw("h");
        }
        else if(file->Get(env.GetValue("name",""))->InheritsFrom("TGraph"))
        {
            cout << "Found graph " << env.GetValue("name","") << endl;
            TGraph* graph = (TGraph*)file->Get(env.GetValue("name",""))->Clone();
            canvas = new TCanvas("canvas_"+TString(env.GetValue("name","")),"canvas_"+TString(env.GetValue("name","")),canvasx,canvasy);
            graph->Draw("apl");
        }
        else if(file->Get(env.GetValue("name",""))->InheritsFrom("TMultiGraph"))
        {
            cout << "Found multigraph " << env.GetValue("name","") << endl;
            TMultiGraph* mg = (TMultiGraph*)file->Get(env.GetValue("name",""))->Clone();
            canvas = new TCanvas("canvas_"+TString(env.GetValue("name","")),"canvas_"+TString(env.GetValue("name","")),canvasx,canvasy);
            mg->Draw("apl");
        }
        else if(file->Get(env.GetValue("name",""))->InheritsFrom("TTree"))
        {
            cout << "Found ntuple " << env.GetValue("name","") << endl;
            TTree* tuple = (TTree*)file->Get(env.GetValue("name",""))->Clone();
            canvas = new TCanvas("canvas_"+TString(env.GetValue("name","")),"canvas_"+TString(env.GetValue("name","")),canvasx,canvasy);
      
            TString var = env.GetValue("var","");
            TString cond = env.GetValue("cond","");

            if(env.Defined("var")) var = env.GetValue("var","");

            if(var=="")
            {
                cout << "No var provided to draw this ntuple!" << endl;
                exit(0);
            }
            if(var.Contains(":"))
            {
                int n = tuple->Draw(var,cond,"goff");
                cout << "n = " << n << endl;
                TGraph* graph = new TGraph(n,tuple->GetVal(1),tuple->GetVal(0));
                graph->SetName("graph_"+TString(env.GetValue("name","")));
                graph->Draw("apl");
            }
            else
            {
                tuple->Draw(var+">>"+env.GetValue("hist","histogram_"+TString::Itoa(which,10))+env.GetValue("binning",""),cond);
                TH1D* histogram = (TH1D*)gROOT->FindObject(env.GetValue("hist","histogram_"+TString::Itoa(which,10)));
                histogram->SetDirectory(0);
                histogram->Draw("hist");
            }
        }
    }

    if(env.GetValue("grabAndGo",false)==true) return canvas;

    gROOT->Reset();
    SetAtlasStyle();
    gStyle->SetPalette(env.GetValue("palette",57));

    canvas->Modified();
    canvas->Update();
    //canvas->Print("test.pdf","pdf");

    canvas->UseCurrentStyle();

    /*
    gPad->SetLogx(env.GetValue("logx",0));
    gPad->SetLogy(env.GetValue("logy",0));
    canvas->Modified();
    */

    TObject *obj;
   
    canvas->GetListOfPrimitives()->ls();
    TIter next(canvas->GetListOfPrimitives());

    int i = 1;

    while ((obj=next()))
    {
        cout << "Reading: " << obj->GetName() << endl;
        if (obj->InheritsFrom("TH1"))
        {
            cout << "histo: " << obj->GetName() << endl;
            ((TH1D*)obj)->SetDirectory(0);

            if(env.Defined("rebin")) ((TH1D*)obj)->Rebin((int)env.GetValue("rebin",1));
            if(env.Defined("normalize")) if(env.GetValue("normalize",0)==1) ((TH1D*)obj)->Scale(1./((TH1D*)obj)->Integral());
            if(env.Defined("scale")) ((TH1D*)obj)->Scale(env.GetValue("scale",1.));
            if(env.Defined("title")) ((TH1D*)obj)->SetTitle(env.GetValue("title",""));
            if(env.Defined("titlex")) ((TH1D*)obj)->GetXaxis()->SetTitle(env.GetValue("titlex",""));
            if(env.Defined("titley")) ((TH1D*)obj)->GetYaxis()->SetTitle(env.GetValue("titley",""));
            if(env.Defined("titlesizex")) ((TH1D*)obj)->GetXaxis()->SetTitleSize(env.GetValue("titlesizex",0.05));
            if(env.Defined("titlesizey")) ((TH1D*)obj)->GetYaxis()->SetTitleSize(env.GetValue("titlesizey",0.05));
            if(env.Defined("titleoffsetx")) ((TH1D*)obj)->GetXaxis()->SetTitleOffset(env.GetValue("titleoffsetx",0.05));
            if(env.Defined("titleoffsety")) ((TH1D*)obj)->GetYaxis()->SetTitleOffset(env.GetValue("titleoffsety",0.05));
            if(env.Defined("labelsizex")) ((TH1D*)obj)->GetXaxis()->SetLabelSize(env.GetValue("labelsizex",0.05));
            if(env.Defined("labelsizey")) ((TH1D*)obj)->GetYaxis()->SetLabelSize(env.GetValue("labelsizey",0.05));
            if(env.Defined("minx") && env.Defined("maxx")) ((TH1D*)obj)->GetXaxis()->SetRangeUser(env.GetValue("minx",0.),env.GetValue("maxx",1000.));
            if(env.Defined("miny")) ((TH1D*)obj)->SetMinimum(env.GetValue("miny",0.01));
            if(env.Defined("maxy")) ((TH1D*)obj)->SetMaximum(env.GetValue("maxy",1000.));
            //if(env.Defined("miny") && env.Defined("maxy")) ((TH1D*)obj)->GetYaxis()->SetLimits(env.GetValue("miny",0.01),env.GetValue("maxy",1000));
            //if(env.Defined("miny") && env.Defined("maxy")) ((TH1D*)obj)->GetYaxis()->SetRangeUser(env.GetValue("miny",0.01),env.GetValue("maxy",1000));
            if(env.Defined("linecolor"))
            {
                if(((TString)env.GetValue("linecolor","")).Contains("auto"))
                {
                    ((TH1D*)obj)->SetLineColor(gStyle->GetColorPalette(which*gStyle->GetNumberOfColors()/total));
                    ((TH1D*)obj)->SetMarkerColor(gStyle->GetColorPalette(which*gStyle->GetNumberOfColors()/total));
                }
                else
                {
                    ((TH1D*)obj)->SetLineColor((int)env.GetValue("linecolor",1));
                    ((TH1D*)obj)->SetMarkerColor((int)env.GetValue("linecolor",1));
                }
            }
            if(env.Defined("linestyle")) ((TH1D*)obj)->SetLineStyle((int)env.GetValue("linestyle",1));
            if(env.Defined("linewidth")) ((TH1D*)obj)->SetLineWidth((int)env.GetValue("linewidth",1));

            ((TH1D*)obj)->SetMarkerSize((int)env.GetValue("markersize",0));
            if(env.Defined("drawoption")) ((TH1D*)obj)->SetDrawOption(env.GetValue("drawoption",""));

            ((TH1D*)obj)->GetXaxis()->SetMoreLogLabels(env.GetValue("moreLogLabelsx",0)==1);
            ((TH1D*)obj)->GetXaxis()->SetNoExponent(env.GetValue("noExponentx",0)==1);

            i++;
        }
        else if(obj->InheritsFrom("TMultiGraph"))
        {
            int whichMgDweller = 0;
            TGraph* mgDweller = 0;
	    TIter mgNext(((TMultiGraph*)obj)->GetListOfGraphs());
	    while ((mgDweller=(TGraph*)mgNext()))
	    {
		cout << "graph: " << mgDweller->GetName() << endl;
		if(env.Defined("title") && TString(((TGraph*)mgDweller)->GetTitle())!="") ((TGraph*)mgDweller)->SetTitle(env.GetValue("title",""));
		if(env.Defined("titlex")) ((TGraph*)mgDweller)->GetXaxis()->SetTitle(env.GetValue("titlex",""));
		if(env.Defined("titley")) ((TGraph*)mgDweller)->GetYaxis()->SetTitle(env.GetValue("titley",""));
                if(env.Defined("titlesizex")) ((TGraph*)mgDweller)->GetXaxis()->SetTitleSize(env.GetValue("titlesizex",0.05));
                if(env.Defined("titlesizey")) ((TGraph*)mgDweller)->GetYaxis()->SetTitleSize(env.GetValue("titlesizey",0.05));
                if(env.Defined("titleoffsetx")) ((TGraph*)mgDweller)->GetXaxis()->SetTitleOffset(env.GetValue("titleoffsetx",0.05));
                if(env.Defined("titleoffsety")) ((TGraph*)mgDweller)->GetYaxis()->SetTitleOffset(env.GetValue("titleoffsety",0.05));
                if(env.Defined("labelsizex")) ((TGraph*)mgDweller)->GetXaxis()->SetLabelSize(env.GetValue("labelsizex",0.05));
                if(env.Defined("labelsizey")) ((TGraph*)mgDweller)->GetYaxis()->SetLabelSize(env.GetValue("labelsizey",0.05));
		if(env.Defined("minx") && env.Defined("maxx")) ((TGraph*)mgDweller)->GetXaxis()->SetLimits(env.GetValue("minx",0.),env.GetValue("maxx",1000.));
		if(env.Defined("minx") && env.Defined("maxx")) ((TGraph*)mgDweller)->GetXaxis()->SetRangeUser(env.GetValue("minx",0.),env.GetValue("maxx",1000.));
		if(env.Defined("miny")) ((TGraph*)mgDweller)->GetHistogram()->SetMinimum(env.GetValue("miny",0.01));
		if(env.Defined("maxy")) ((TGraph*)mgDweller)->GetHistogram()->SetMaximum(env.GetValue("maxy",1000.));
		if(env.Defined("miny") && env.Defined("maxy")) ((TGraph*)mgDweller)->GetYaxis()->SetLimits(env.GetValue("miny",0.01),env.GetValue("maxy",1000.));
		if(env.Defined("miny") && env.Defined("maxy")) ((TGraph*)mgDweller)->GetYaxis()->SetRangeUser(env.GetValue("miny",0.01),env.GetValue("maxy",1000.));
		if(env.Defined("linecolor"))
		{
		    if(((TString)env.GetValue("linecolor","")).Contains("auto"))
		    {
			((TGraph*)mgDweller)->SetLineColor(gStyle->GetColorPalette((which+whichMgDweller)*gStyle->GetNumberOfColors()/(total + ((TMultiGraph*)obj)->GetListOfGraphs()->GetEntries())));
			((TGraph*)mgDweller)->SetMarkerColor(gStyle->GetColorPalette((which+whichMgDweller)*gStyle->GetNumberOfColors()/(total + ((TMultiGraph*)obj)->GetListOfGraphs()->GetEntries())));
		    }
		    else ((TGraph*)mgDweller)->SetLineColor((int)env.GetValue("linecolor",1));
		}
		if(env.Defined("linestyle")) ((TGraph*)mgDweller)->SetLineStyle((int)env.GetValue("linestyle",1));
                if(env.Defined("linewidth")) ((TGraph*)mgDweller)->SetLineWidth((int)env.GetValue("linewidth",1));
		((TGraph*)mgDweller)->SetMarkerSize((int)env.GetValue("markersize",0));

		((TGraph*)mgDweller)->GetXaxis()->SetMoreLogLabels(env.GetValue("moreLogLabelsx",0)==1);
		((TGraph*)mgDweller)->GetXaxis()->SetNoExponent(env.GetValue("noExponentx",0)==1);

		canvas->Modified();
		canvas->Update();

                whichMgDweller++;
		i++;

            }
        }
        else if(obj->InheritsFrom("TGraph"))
        {
            cout << "graph: " << obj->GetName() << endl;
            if(env.Defined("title") && TString(((TGraph*)obj)->GetTitle())!="") ((TGraph*)obj)->SetTitle(env.GetValue("title",""));
            if(env.Defined("titlex")) ((TGraph*)obj)->GetXaxis()->SetTitle(env.GetValue("titlex",""));
            if(env.Defined("titley")) ((TGraph*)obj)->GetYaxis()->SetTitle(env.GetValue("titley",""));
            if(env.Defined("titlesizex")) ((TGraph*)obj)->GetXaxis()->SetTitleSize(env.GetValue("titlesizex",0.05));
            if(env.Defined("titlesizey")) ((TGraph*)obj)->GetYaxis()->SetTitleSize(env.GetValue("titlesizey",0.05));
            if(env.Defined("titleoffsetx")) ((TGraph*)obj)->GetXaxis()->SetTitleOffset(env.GetValue("titleoffsetx",0.05));
            if(env.Defined("titleoffsety")) ((TGraph*)obj)->GetYaxis()->SetTitleOffset(env.GetValue("titleoffsety",0.05));
            if(env.Defined("labelsizex")) ((TGraph*)obj)->GetXaxis()->SetLabelSize(env.GetValue("labelsizex",0.05));
            if(env.Defined("labelsizey")) ((TGraph*)obj)->GetYaxis()->SetLabelSize(env.GetValue("labelsizey",0.05));
            if(env.Defined("minx") && env.Defined("maxx")) ((TGraph*)obj)->GetXaxis()->SetLimits(env.GetValue("minx",0.),env.GetValue("maxx",1000.));
            if(env.Defined("minx") && env.Defined("maxx")) ((TGraph*)obj)->GetXaxis()->SetRangeUser(env.GetValue("minx",0.),env.GetValue("maxx",1000.));
            if(env.Defined("miny")) ((TGraph*)obj)->GetHistogram()->SetMinimum(env.GetValue("miny",0.01));
            if(env.Defined("maxy")) ((TGraph*)obj)->GetHistogram()->SetMaximum(env.GetValue("maxy",1000.));
            if(env.Defined("miny") && env.Defined("maxy")) ((TGraph*)obj)->GetYaxis()->SetLimits(env.GetValue("miny",0.01),env.GetValue("maxy",1000.));
            if(env.Defined("miny") && env.Defined("maxy")) ((TGraph*)obj)->GetYaxis()->SetRangeUser(env.GetValue("miny",0.01),env.GetValue("maxy",1000.));
            if(env.Defined("linecolor"))
            {
                if(((TString)env.GetValue("linecolor","")).Contains("auto"))
                {
                    ((TGraph*)obj)->SetLineColor(gStyle->GetColorPalette(which*gStyle->GetNumberOfColors()/total));
                    ((TGraph*)obj)->SetMarkerColor(gStyle->GetColorPalette(which*gStyle->GetNumberOfColors()/total));
                }
                else
                {
                    ((TGraph*)obj)->SetLineColor((int)env.GetValue("linecolor",1));
                    ((TGraph*)obj)->SetMarkerColor((int)env.GetValue("linecolor",1));
                }        
            }
            if(env.Defined("linestyle")) ((TGraph*)obj)->SetLineStyle((int)env.GetValue("linestyle",1));
            if(env.Defined("linewidth")) ((TGraph*)obj)->SetLineWidth((int)env.GetValue("linewidth",1));
            double defaultMarkerSize = ((TGraph*)obj)->GetN() < 2 ? 1. : 0;
            ((TGraph*)obj)->SetMarkerSize(env.GetValue("markersize",defaultMarkerSize));
            if(env.Defined("drawoption")) ((TGraph*)obj)->SetDrawOption(env.GetValue("drawoption",""));

            ((TGraph*)obj)->GetXaxis()->SetMoreLogLabels(env.GetValue("moreLogLabelsx",0)==1);
            ((TGraph*)obj)->GetXaxis()->SetNoExponent(env.GetValue("noExponentx",0)==1);

            ((TGraph*)obj)->GetYaxis()->SetMoreLogLabels(env.GetValue("moreLogLabelsy",0)==1);
            ((TGraph*)obj)->GetYaxis()->SetNoExponent(env.GetValue("noExponenty",0)==1);

            //((TGraph*)obj)->Draw(TString(env.GetValue("drawoption","")) + " A");
            canvas->Modified();
            canvas->Update();

            i++;
        }
        else if(obj->InheritsFrom("TF1"))
        {
            cout << "function: " << obj->GetName() << endl;
            if(env.Defined("title")) ((TF1*)obj)->SetTitle(env.GetValue("title",""));
            if(env.Defined("titlex")) ((TF1*)obj)->GetXaxis()->SetTitle(env.GetValue("titlex",""));
            if(env.Defined("titley")) ((TF1*)obj)->GetYaxis()->SetTitle(env.GetValue("titley",""));
            if(env.Defined("minx") && env.Defined("maxx")) ((TF1*)obj)->GetXaxis()->SetLimits(env.GetValue("minx",0.),env.GetValue("maxx",1000.));
            if(env.Defined("minx") && env.Defined("maxx")) ((TF1*)obj)->GetXaxis()->SetRangeUser(env.GetValue("minx",0.),env.GetValue("maxx",1000.));
            if(env.Defined("miny")) ((TF1*)obj)->GetHistogram()->SetMinimum(env.GetValue("miny",0.01));
            if(env.Defined("maxy")) ((TF1*)obj)->GetHistogram()->SetMaximum(env.GetValue("maxy",1000.));
            if(env.Defined("miny") && env.Defined("maxy")) ((TF1*)obj)->GetYaxis()->SetLimits(env.GetValue("miny",0.01),env.GetValue("maxy",1000.));
            if(env.Defined("miny") && env.Defined("maxy")) ((TF1*)obj)->GetYaxis()->SetRangeUser(env.GetValue("miny",0.01),env.GetValue("maxy",1000.));
            if(env.Defined("linecolor"))
            {
                if(((TString)env.GetValue("linecolor","")).Contains("auto"))
                {
                    ((TF1*)obj)->SetLineColor(gStyle->GetColorPalette(which*gStyle->GetNumberOfColors()/total));
                    ((TF1*)obj)->SetMarkerColor(gStyle->GetColorPalette(which*gStyle->GetNumberOfColors()/total));
                }
                else ((TF1*)obj)->SetLineColor((int)env.GetValue("linecolor",1));
            }
            if(env.Defined("linestyle")) ((TF1*)obj)->SetLineStyle((int)env.GetValue("linestyle",1));

            ((TF1*)obj)->GetXaxis()->SetMoreLogLabels(env.GetValue("moreLogLabelsx",0)==1);
            ((TF1*)obj)->GetXaxis()->SetNoExponent(env.GetValue("noExponentx",0)==1);

            canvas->Modified();
            canvas->Update();

            i++;
        }
    }

    return canvas;
}

TCanvas* canvascombiner(TObjArray* canArray, TString card)
{
    TEnv env(card);

    int canvasx = env.GetValue("canvasx",800);
    int canvasy = env.GetValue("canvasy",600);

    TCanvas* combinedCanvas = new TCanvas("combinedCanvas","combinedCanvas",canvasx,canvasy);

    combinedCanvas->SetTopMargin(env.GetValue("margintop",0.05));
    combinedCanvas->SetRightMargin(env.GetValue("marginright",0.05));
    combinedCanvas->SetBottomMargin(env.GetValue("marginbottom",0.16));
    combinedCanvas->SetLeftMargin(env.GetValue("marginleft",0.16));

    TLegend* combinedLegend = new TLegend(env.GetValue("legendminx",0.22),env.GetValue("legendminy",0.7),env.GetValue("legendmaxx",0.42),env.GetValue("legendmaxy",0.9));

    combinedLegend->SetNColumns(env.GetValue("legendcolumns",1));
    combinedLegend->SetBorderSize(0); combinedLegend->SetFillStyle(0); combinedLegend->SetTextFont(42); combinedLegend->SetTextSize(env.GetValue("legendtextsize",0.040));
    if(env.Defined("legendmargin")) combinedLegend->SetMargin(env.GetValue("legendmargin",0.));

    TIter canIter(canArray);

    int i=0;
    while(TCanvas* canvas = (TCanvas*)canIter())
    {
        cout << "Looking for objects in the canvas called "<< canvas->GetName() << endl;

        TObject *obj;
        canvas->GetListOfPrimitives()->ls();
        TIter objIter((TObjArray*)canvas->GetListOfPrimitives());

        int j=0;
        while ((obj = objIter()))
        {
            TString newName = "";

            if (obj->IsA()->InheritsFrom("TH1"))
            {
                newName = "hist_" + TString::Itoa(i,10) + "_" + TString::Itoa(i,10);
                //newName = "M_{LQ} = " + TString(canvas->GetName()).ReplaceAll("canvas_","") + " GeV";
                cout << "TH1: " << obj->GetName() << " --> " << newName << endl;
                ((TH1D*)obj)->SetName(newName);
                combinedCanvas->cd();
                TString drawOpt = ((TH1D*)obj)->GetDrawOption();
                //((TH1D*)obj)->Draw(i==0 && j==0 ? drawOpt : drawOpt + " same");
                if(i==0 && j==0) ((TH1D*)obj)->Draw("hist");
                else ((TH1D*)obj)->Draw("hist same");
                if((TString)obj->GetTitle()!="") combinedLegend->AddEntry(obj->GetName(),obj->GetTitle(), "l");
                j++;
            }

            else if (obj->IsA()->InheritsFrom("TMultiGraph"))
            {
                int k=0;
                TGraph* mgDweller;
                TIter mgNext(((TMultiGraph*)obj)->GetListOfGraphs());
                while ((mgDweller=(TGraph*)mgNext()))
                {
                    newName = "graph_" + TString::Itoa(i,10) + "_" + TString::Itoa(j,10) + "_" + TString::Itoa(k,10);
                    cout << "TGraph: " << obj->GetName() << " --> " << newName << endl;
                    mgDweller->SetName(newName);
                    combinedCanvas->cd();
                    mgDweller->Draw(i==0 && j==0 && k==0 ? "AL" : "same");
                    if((TString)mgDweller->GetTitle()!="") combinedLegend->AddEntry(mgDweller->GetName(),mgDweller->GetTitle(), "L");
                    k++;
                }
            }

            else if (obj->IsA()->InheritsFrom("TGraph"))
            {
                newName = "graph_" + TString::Itoa(i,10) + "_" + TString::Itoa(j,10);
                //newName = "M_{LQ} = " + TString(canvas->GetName()).ReplaceAll("canvas_","") + " GeV";
                cout << "TGraph: " << obj->GetName() << " --> " << newName << endl;
                ((TGraph*)obj)->SetName(newName);
                cout << "the y-title is " << ((TGraph*)obj)->GetYaxis()->GetTitle() << endl;
                combinedCanvas->cd();
                TString drawOpt = ((TGraph*)obj)->GetDrawOption();
                if(drawOpt=="")
                {
                    cout << "the drawoption is blank (= " << ((TGraph*)obj)->GetDrawOption() << "); going to use default..." << endl;
                    ((TGraph*)obj)->GetN() < 2 ? drawOpt = "P" : drawOpt = "L";
                }
                if(i==0 && j==0) ((TGraph*)obj)->Draw(drawOpt + " A");
                else
                {
                    ((TGraph*)obj)->GetXaxis()->SetTitle("");
                    ((TGraph*)obj)->GetYaxis()->SetTitle("");
                    ((TGraph*)obj)->Draw( drawOpt + " same");
                }
                if((TString)obj->GetTitle()!="") combinedLegend->AddEntry(obj->GetName(),obj->GetTitle(), drawOpt);
                j++;
            }

            else if (obj->IsA()->InheritsFrom("TF1"))
            {
                newName = "function_" + TString::Itoa(i,10) + "_" + TString::Itoa(i,10);
                //newName = "M_{LQ} = " + TString(canvas->GetName()).ReplaceAll("canvas_","") + " GeV";
                cout << "TF1: " << obj->GetName() << " --> " << newName << endl;
                ((TF1*)obj)->SetName(newName);
                combinedCanvas->cd();
                ((TF1*)obj)->Draw(i==0 && j==0 ? "AL" : "same");
                if((TString)obj->GetTitle()!="") combinedLegend->AddEntry(obj->GetName(),obj->GetTitle(), "L");
                j++;
            }

            else
            {
                cout << "TObject: " << obj->GetName() << endl;
            }
        }

        i++;
    }

    combinedLegend->Draw();

    return combinedCanvas;
}

void canvaswriter(TCanvas* canvas, TString outName)
{
    TString pdfName = outName;
    pdfName.ReplaceAll(".root",".pdf");

    TString cName = outName;
    cName.ReplaceAll(".root",".C");

    cout << "\n\nWriting to file\n\t" << pdfName << "\nand\n\t" << outName << endl;

    canvas->Print(pdfName,"pdf");
    //canvas->Print(cName,"C");

    TFile* outFile = new TFile(outName,"recreate");
    outFile->cd();
    canvas->Write();

    TObject *obj;
    TIter objIter((TObjArray*)canvas->GetListOfPrimitives());

    while ((obj = objIter()))
    {
        if (obj->IsA()->InheritsFrom("TH1") || obj->IsA()->InheritsFrom("TGraph"))
        {
            outFile->cd();
            obj->Write();
        }
    }
}
